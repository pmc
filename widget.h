#ifndef _WIDGET_H
#define _WIDGET_H

typedef struct _widget {
	WINDOW *win;
	void (*redraw)(struct _widget);
	void *pdata;
	int xpos;
	int width;
} widget_t;

typedef void (*bw_redraw)(WINDOW *w, void *pdata, int width);



#endif
