#ifndef _COLOR_H
#define _COLOR_H

typedef enum {
	C_BLACK = 0,
	C_RED = 1,
	C_GREEN = 2,
	C_YELLOW = 3,
	C_BLUE = 4,
	C_MAGENTA = 5,
	C_CYAN = 6,
	C_WHITE = 7
} pmc_color_t;

#define C_COLORMIN C_BLACK
#define C_COLORMAX C_WHITE

#define BG(x) ((short)((x<<3)&(0x38)))

void pmc_init_color(void);

#endif
