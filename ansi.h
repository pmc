#ifndef _ANSI_H
#define _ANSI_H

size_t ansi_waddnstr(WINDOW *win, const char *buffer, size_t len);
int ansi_strip(char *circbuf, const char *buffer, size_t len);

#endif
