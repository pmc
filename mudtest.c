#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ncurses.h>
#include <signal.h>

#include <cbuf.h>
#include <mudio.h>
#include <screen.h>
#include <debug.h>


static int need_resize = 0;

static void
sig_resize(int sig)
{
	need_resize = 1;
}

void
do_refresh(void *win, mud_conn_t *conn)
{
	if (cb_length(conn->input_buffer)) {
		pmc_buffer(win, conn->input_buffer, cb_length(conn->input_buffer));
		cb_reset(conn->input_buffer);
	} 
	pmc_update(win);
	doupdate();
}

int
main(int argc, const char **argv)
{
	void *win;
	fd_set rfds;
	mud_conn_t *conn;
	char buf[80];
	char rtt[20];
	int ret;

	//debug_open();

	if (argc < 3) {
		printf("usage: %s host port %d\n", argv[0], COLOR_PAIRS);
		return 1;
	}

	if (argc == 3) {
		conn = mud_create("name", argv[1], atoi(argv[2]), 4096, 4096);
		if (!conn) {
			printf("Failed to connect to %s:%s: %s\n",
			       argv[1], argv[2], strerror(errno));
			return 1;
		}
	} else if (argc == 4) {
		conn = mud_connect("name", argv[1], atoi(argv[2]), 4096, 4096, atoi(argv[3]));
		if (!conn) {
			printf("Failed to reconnect to %s:%s: %s\n",
			       argv[1], argv[2], strerror(errno));
			return 1;
		}
	}

	signal(SIGWINCH, sig_resize);
	signal(SIGPIPE, SIG_IGN);

	win = pmc_init();

	snprintf(buf, sizeof(buf), " %s:%s - pmc", argv[1], argv[2]);
	pmc_write_bar(win, BAR_TOP, buf);

	while (1) {
		FD_ZERO(&rfds);
		FD_SET(0, &rfds);
		FD_SET(conn->sockfd, &rfds);

		do_refresh(win,conn);

		ret = select(conn->sockfd+1, &rfds, 0, 0, NULL);
		if (need_resize == 1) {
			pmc_resize(win);
			need_resize = 0;
			continue;
		}
		if (FD_ISSET(0, &rfds)) {

			ret = pmc_listen(win);
			if (ret == 1) {
				pmc_copyout(win, conn->output_buffer);
				if (!strcmp(conn->output_buffer, "/log\n")) {
					FILE *f;
					f = fopen("foo.log", "w+");
					fprintf(f, "%s\n", pmc_get_scrollback(win));
					cb_reset(conn->output_buffer);
					pmc_write_bar(win, BAR_BOTTOM, "Dumped Log");
					fclose(f);
				}
				if (!strcmp(conn->output_buffer, "/restart\n")) {
					char fdbuf[10];
					pmc_write_bar(win, BAR_BOTTOM, " RESTARTING ");
					do_refresh(win,conn);
					sleep(1);

					endwin();
					snprintf(fdbuf, sizeof(fdbuf), "%d", conn->sockfd);
					execlp(argv[0], argv[0], argv[1], argv[2],
						fdbuf, NULL);
				}
			}
		}

		if (mud_io(conn) < 0)
			break;

		snprintf(rtt, sizeof(rtt), " RTT %5d", conn->rtt);
		pmc_write_bar(win, BAR_BOTTOM, rtt);

	}

	pmc_write_bar(win, BAR_BOTTOM, "Press any key to exit");
	do_refresh(win,conn);

	getch();

	endwin();

	return 0;
}
