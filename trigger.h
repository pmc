#ifndef _TRIGGER_H
#define _TRIGGER_H

typedef enum {
	TRIGGER_SIMPLE,
	TRIGGER_GLOB,
	TRIGGER_REGEX
} trigger_match_t;

typedef int (*trigger_regex_t)(void *s, regmatch_t matches[], size_t nmatch);

typedef int (*trigger_simple_t)(void *s, const char *line);

typedef struct {
	char *name;
	trigger_match_t match;
	union u {
		regex_t rx;
		char *match;
	};
	union f {
		trigger_regex_t rx;
		trigger_simple_t simple;
	};
} trigger_t;

trigger_t *trig_new(const char *name,
		    trigger_type_t type,
		    const char *match_pattern,
		    size_t buffer_size,
		    int flags);

int trig_check(void *s, trigger_t *trig, const char *scrollback, const char *line);

void trig_free(trigger_t *done);
		      

#endif
