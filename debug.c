#include <debug.h>

FILE *_debug = NULL;

int
debug_open(void)
{
	FILE *fp;

	fp = fopen("debug.out", "a+");
	if (!fp)
		return -1;
	_debug = fp;
	return 0;
}


void
debug_close(void)
{
	if (_debug) {
		fclose(_debug);
		_debug = NULL;
	}
}

