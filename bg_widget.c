#include <widget.h>

#define BG_MAGIC 0x10101343

struct bar_graph_info {
	int magic;
	attr_t f_attr;
	short f_color;
	attr_t e_attr;
	short e_color;
	int curr;
	int max;
};

widget_t *
wid_bg_new(WINDOW *win, attr_t f_attr, short f_color, attr_t e_attr, short e_color)
{
}

void
wid_bg_set(widget_t *wid, int curr, int max)
{
}


void
wid_bg_redraw(widget_t *wid)
{
	struct bar_graph_info *b = (struct bar_graph_info *)wid->pdata;
	attr_t attr;
	short color;

	if (!wid || wid->magic != WIDGET_MAGIC)
		return;
	if (!b || b->magic != BG_MAGIC)
		return;

	/* Save for later */
	wattr_get(wid->window, &attr, &color, NULL);

}
