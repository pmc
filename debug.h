#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

int debug_open(void);
void debug_close(void);

#define debug_print(fmt, args...) \
if (_debug) do {\
	fprintf(_debug, fmt, ##args); \
	fflush(_debug); \
} while(0)

extern FILE *_debug;

#endif
