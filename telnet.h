#ifndef _TELNET_H
#define _TELNET_H

/* RFC854 */

#define TN_SE		240
#define TN_NOP		241
#define TN_MARK		242
#define TN_BREAK	243
#define TN_INTR		244
#define TN_ABORT	245
#define TN_AYT		246
#define TN_ERASE_CHAR	247
#define TN_ERASE_LINE	248
#define TN_GO_AHEAD	249
#define TN_SB		250
#define TN_WILL		251
#define TN_WONT		252
#define TN_DO		253
#define TN_DONT		254
#define TN_IAC		255

#define TN_ESCAPE	255

/* RFC857 - echo control */

#define TN_OPT_ECHO	1

#endif
