#ifndef _CONNECT_H
#define _CONNECT_H

/* ipv4 only for now */

#define RTT_HIST_LEN 10

typedef struct {
	char *name;
	char *host;
	char *input_buffer;
	char *output_buffer;
	struct timeval last_send;
	int port;
	int sockfd;
	int flags;
#define FL_ECHO 0x1
	int rtt_hist[RTT_HIST_LEN];
	int rtt;		/* Est. latency in ms */
	int rtt_idx;
} mud_conn_t;

mud_conn_t *mud_create(const char *name,
		       const char *host,
		       int port,
		       size_t in_bufsz, size_t out_bufsz);

mud_conn_t *mud_connect(const char *name,
			const char *host,
			int port,
			size_t in_bufsz, size_t out_bufsz, int fd);

int mud_io(mud_conn_t *mud);

#endif
