#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <cbuf.h>
#include <ncurses.h>
#include <stdlib.h>

#include <color.h>
#include <ansi.h>

#ifdef DEBUG

int
main(int argc, char **argv)
{
	size_t nbytes = 0;
	char buf[1024];
	char buf2[1024];
	char *cb;
	WINDOW *bg;
	FILE *fp = fopen(argv[1], "r");

	if (!fp) {
		printf(" needs filename dummy\n");
		return 1;
	}

	if (argc == 3) {
		cb = cb_init(buf2, sizeof(buf2), NULL);
		while (fgets(buf, sizeof(buf), fp)!=NULL) {
			cb_reset(cb);
			ansi_strip(cb, buf, strlen(buf));
			printf("%s", cb);
		}

		return 0;
	}




	bg = initscr();
	start_color();
	pmc_init_color();
	scrollok(bg, TRUE);
	clearok(bg, FALSE);

	wattr_set(bg, A_NORMAL, (short)(BG(C_BLACK)|C_WHITE), NULL);

	while (fgets(buf, sizeof(buf), fp)!=NULL) {
		nbytes += ansi_waddnstr(bg, buf, strlen(buf));
		wnoutrefresh(bg);
		doupdate();
	}

	getch();
	endwin();
	printf("%d bytes\n", nbytes);
	return 0;
}
#endif

