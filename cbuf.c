/*
   Simple circular C-string buffer
   TODO: use page size memcopy for performance; shift by whole page
	 sizes at a time instead of a couple of bytes.
   TODO: cb_printf()

   Basically, the contents of a circular buffer _always_ can be 
   acted on by standard GNU C string functions and regex
   functions.

   The buffers never grow in size, and provided you use the
   cb_append() or cb_strcat() functions, you cannot overrun them.

    -- Lon
 */
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <stdint.h>
#include <cbuf.h>
#include <malloc.h>
#include <unistd.h>

#define CB_MAGIC 0x133419A

struct circ_buffer {
	uint32_t magic;
	uint32_t pad;
	size_t max;
	size_t len;
};

#ifdef DEBUG
#define CB_END_MAGIC 0x8014abcc
#define CB_SIZE (sizeof(struct circ_buffer) + sizeof(uint32_t))
#include <stdio.h>
#include <fcntl.h>
#define EXTRA_VALIDATE(cb, buffer) \
do { \
	if ( *((uint32_t *)(buffer + cb->max + 1)) != CB_END_MAGIC) \
		printf("Memory for cbuf %p corrupted\n", cb); \
} while(0)

#else

#define CB_SIZE (sizeof(struct circ_buffer) )
#define EXTRA_VALIDATE(buffer, cb)

#endif

#define DECODE(cb, buffer, args...) \
do { \
	errno = EINVAL; \
	if (!buffer) \
		return args; \
	cb = (struct circ_buffer *)(buffer - sizeof(struct circ_buffer)); \
	if (cb->magic != CB_MAGIC) \
		return args; \
	EXTRA_VALIDATE(cb, buffer); \
} while(0)

/* Always null-terminate */
#define CB_MINSIZE (CB_SIZE + 1)

/* ======================================================= */

/* Returns # of bytes appended or -1 on error */
int
cb_append(char *buffer, const char *data, size_t len)
{
	struct circ_buffer *cb;
	size_t olen;

	if (!data || !len)
		return 0;

	DECODE(cb, buffer, -1);

	/* Too long for our buffer; truncate and get out */
	if (len > cb->max) {
		memcpy(buffer, data + (len - cb->max), cb->max);
		cb->len = cb->max;
		return cb->max;
	}

	/* Not enough space - memmove to make space
	   and copy in last bits */
	if ((cb->len + len) > cb->max) {
		olen = cb->len + len - cb->max;
		memmove(buffer, buffer + olen, cb->max - olen);
		cb->len -= olen;
	}

	memcpy(buffer + cb->len, data, len);
	cb->len += len;
	buffer[cb->len] = 0;
	return len;
}


static int 
_cb_delete(struct circ_buffer *cb,
	    char *buffer, char *spot, size_t len)
{
	size_t olen;

	if (!len)
		return 0;
	if (len > cb->len)
		return 0;

	olen = cb->len;
	olen -= (spot - buffer);
	if (len > olen)
		len = olen;
	memmove(spot, spot+len, (olen - len));
	cb->len -= len;
	buffer[cb->len] = 0;

	return (int)len;
}


int
cb_delete(char *buffer, char *spot, size_t len)
{
	struct circ_buffer *cb;

	if (!spot || !len)
		return 0;

	DECODE(cb, buffer, -1);

	return _cb_delete(cb, buffer, spot, len);
}


int
cb_backspace(char *buffer, size_t len)
{
	struct circ_buffer *cb;

	if (!len)
		return 0;

	DECODE(cb, buffer, -1);

	return _cb_delete(cb, buffer, &buffer[cb->len-len], len);
}


/* convenience function; operates like strcat, incl. return value */
char *
cb_strcat(char *buffer, const char *data)
{
	if (cb_append(buffer, data, strlen(data)) > 0)
		return buffer;
	return NULL;
}


/* Create a new circular buffer from user-allocated or stack
   memory passed in at *start */
char *
cb_init(void *start, size_t len, size_t *retlen)
{
	struct circ_buffer *ret;
	char *cooked;

	if (!start) {
		errno = EINVAL;
		return NULL;
	}

	if (len < CB_MINSIZE) {
		errno = ENOMEM; 
		return NULL;
	}

	memset(start, 0, len);

	ret = (struct circ_buffer *)start;
	ret->max = len - (CB_MINSIZE); /* header and trailer, if DEBUG */
	ret->magic = CB_MAGIC;

	cooked = (char *)(&ret[1]);
#ifdef DEBUG
	*((uint32_t *)(cooked + ret->max + 1)) = CB_END_MAGIC;
#endif

	if (retlen)
		*retlen = ret->max;

	return cooked;
}


/* Clear the entire buffer */
static inline void
_reset(struct circ_buffer *cb)
{
	memset(&cb[1], 0, cb->max);
	cb->len = 0;
}


int
cb_reset(char *buffer)
{
	struct circ_buffer *cb;

	DECODE(cb, buffer, -1);

	_reset(cb);
	return 0;
}


/* Take the first count bytes off the front of the buffer */
int
cb_discard(char *buffer, size_t count)
{
	struct circ_buffer *cb;

	DECODE(cb, buffer, -1);

	if (count >= cb->max) {
		_reset(cb);
		return cb->max;
	}

	return _cb_delete(cb, buffer, buffer, count);
}


/* Invalidate a circular buffer and return the pointer the
   user fed to us when he/she called cb_init() so they may
   free it */
void *
cb_destroy(char *buffer)
{
	struct circ_buffer *cb;

	DECODE(cb, buffer, NULL);

	cb->magic = 0;
	return (void *)cb;
}


/* By design, users can use strlen() on the buffer.  This
   allows use of binary data if desired */
size_t
cb_length(char *buffer)
{
	struct circ_buffer *cb;

	DECODE(cb, buffer, 0);

	return cb->len;
}


size_t
cb_minsize(void)
{
	return CB_MINSIZE;
}


void
cb_free(char *buffer)
{
	struct circ_buffer *cb;

	DECODE(cb, buffer);

	free(cb);
}
