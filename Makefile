all: mudtest

cbuftest: cbuf.o cbuftest.o
	gcc -o $@ $^ -ggdb

ansitest: color.o ansi.o ansitest.o cbuf.o
	gcc -o $@ $^ -ggdb -lncurses

mudtest: cbuf.o mudio.o mudtest.o screen.o color.o ansi.o debug.o
	gcc -o $@ $^ -ggdb -lncurses

screentest: screen.o cbuf.o
	gcc -o $@ $^ -ggdb -lncurses

clean:
	rm -f *.o *~ cbuf *test

%.o: %.c
	gcc -c -o $@ $^ -I. -Wstrict-prototypes -Wshadow -Wall -ggdb -DDEBUG
