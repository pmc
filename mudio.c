#include <string.h>
#include <sys/types.h>
#include <malloc.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>

#include <mudio.h>
#include <cbuf.h>


mud_conn_t *
mud_connect(const char *name,
	    const char *host,
	    int port,
	    size_t in_bufsz, size_t out_bufsz, int fd)
{
	mud_conn_t *mud;
	struct sockaddr_in sin;
	struct addrinfo *ai;
	struct addrinfo ai_hint;
	int ret, esv;

	memset(&ai_hint, 0, sizeof(ai_hint));

	errno = ENOMEM;
	mud = malloc (sizeof(*mud));
	if (!mud)
		return NULL;
	memset(mud, 0, sizeof(*mud));
	mud->input_buffer = malloc(in_bufsz);
	mud->output_buffer = malloc(out_bufsz);
	if (!mud->input_buffer || !mud->output_buffer) {
		goto out_free;
	}

	mud->input_buffer = cb_init(mud->input_buffer, in_bufsz, NULL);
	mud->output_buffer = cb_init(mud->output_buffer, out_bufsz, NULL);
	mud->name = strdup(name);
	mud->host = strdup(host);
	mud->port = port;
	mud->flags = FL_ECHO;

	mud->sockfd = fd;

	if (fd < 0) {
		mud->sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (mud->sockfd < 0) {
			fprintf(stderr, "socket: %s\n", strerror(errno));
			goto out_free;
		}

		memset(&ai_hint,0, sizeof(ai_hint));
		ai_hint.ai_family = PF_INET;
		ret = getaddrinfo(host, NULL, &ai_hint, &ai);
		if (ret != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
			goto out_free;
		}

		memcpy(&sin, ai->ai_addr, sizeof(sin));
		sin.sin_family = PF_INET;
		sin.sin_port = htons(port);

		freeaddrinfo(ai);

		if (connect(mud->sockfd, (struct sockaddr *)&sin,
		    	(socklen_t)(sizeof(sin))) < 0) {
			fprintf(stderr, "connect: %s\n", strerror(errno));
			goto out_free;
		}
	}

	return mud;

out_free:
	esv = errno;
	if (mud->sockfd >= 0)
		close(mud->sockfd);
	free(mud->name);
	free(mud->host);
	cb_free(mud->input_buffer);
	cb_free(mud->output_buffer);
	free(mud);
	errno = esv;
	return NULL;
}


mud_conn_t *
mud_create(const char *name,
	   const char *host,
	   int port,
	   size_t in_bufsz, size_t out_bufsz)
{
	return mud_connect(name, host, port, in_bufsz, out_bufsz, -1);
}


#if 0
void
do_telnet_req(mud_conn_t *mud, buf[x]);
{
	case ''
}


int
telnet_scan_buf(mud_conn_t *mud, char *buf, size_t len, size_t curr)
{
	int x;
	int needed = 0;
	int in_code = 0;
	int req = 0;

retry:
	for (x = 0; x < curr; x++) {
		if (needed)
			needed--;

		if (req && in_code == 2) {
			do_req(mud, req, buf[x]);
		}

		switch(buf[x]) {
		case TN_ESCAPE:
			if (in_code) {
				in_code = 0;
				/* output 0xff */
			} else {
				in_code = 1;
				needed = 1;
			}
			break;
		case TN_DO:
		case TN_DONT:
		case TN_WILL:
		case TN_WONT:
			if (in_code) {
				req = buf[x];
				in_code = 2;
				needed = 1;
			}
			break;
		}

	
}

#endif

static int
delta_est(struct timeval *old, struct timeval *now)
{
	struct timeval tv;

	if (now->tv_usec > old->tv_usec) {
		old->tv_usec += 1000000;
		old->tv_sec--;
	}
	tv.tv_sec = now->tv_sec - old->tv_sec;
	tv.tv_usec = now->tv_usec - old->tv_usec;
	
	return ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}


static void
est_rtt(mud_conn_t *mud, struct timeval *prev, struct timeval *now)
{
	int x, total = 0;
	mud->rtt_hist[mud->rtt_idx] = delta_est(prev, now);
	mud->rtt_idx++;
	if (mud->rtt_idx >= RTT_HIST_LEN)
		mud->rtt_idx = 0;

	for (x = 0; x < RTT_HIST_LEN; x++) {
		if (mud->rtt_hist[x] == 0)
			break;
		total += mud->rtt_hist[x];
	}

	mud->rtt = (total / x);
}


int
mud_io(mud_conn_t *mud)
{
	char buf[4096]; // XXX _sysconf(_SC_PAGESIZE)
	struct pollfd fd;
	int ret, total = 0, timeout = 0, written = 0;
	struct timeval tv;

	if (mud->sockfd < 0)
		goto out_conn;

	memset(&fd, 0, sizeof(fd));
	fd.fd = mud->sockfd;
	fd.events = POLLIN | POLLERR | POLLHUP;

	if (cb_length(mud->output_buffer))
		fd.events |= POLLOUT;

	switch (poll(&fd, 1, timeout)) {
	case -1:
		return -1;
	case 0:
		return 0;
	default:
		break;
	}

	if (fd.revents & (POLLERR|POLLHUP)) {
		goto out_conn;
	}

	if ((fd.revents & POLLOUT) && cb_length(mud->output_buffer)) {
		written = write(mud->sockfd,
				mud->output_buffer,
				cb_length(mud->output_buffer));
		if (written > 0) {
			gettimeofday(&mud->last_send, NULL);
			cb_discard(mud->output_buffer, (size_t)written);
		}
	}

	if (!(fd.revents & POLLIN))
		return 0;

	do {
		ret = read(mud->sockfd,
			   buf, sizeof(buf));
		if (ret < 0) {
			if (errno == EINTR)
				return 0;
			else {
				goto out_conn;
			}
		}

		//telnet_scan_buf(mud, buf, sizeof(buf), ret);

		if (ret > 0) {
			if (!written && mud->last_send.tv_sec) {
				gettimeofday(&tv, NULL);
				est_rtt(mud, &mud->last_send, &tv);
				mud->last_send.tv_sec = 0;
			}
			cb_append(mud->input_buffer, buf, ret);
		}

		total += ret;
		if (total == 0 && ret == 0)
			/* went high but no data? lost conn (only on some systems) */
			goto out_conn;
	} while (ret == sizeof(buf));

	return total;

out_conn:
	if (mud->sockfd >= 0)
		close(mud->sockfd);
	errno = ECONNRESET;
	cb_strcat(mud->input_buffer, "\n\nNO CARRIER\n");
	mud->sockfd = -1;
	return -1;
}
