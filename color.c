#include <ncurses.h>
#include <color.h>


int pmc_to_curses[] = {
	COLOR_BLACK,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_YELLOW,
	COLOR_BLUE,
	COLOR_MAGENTA,
	COLOR_CYAN,
	COLOR_WHITE
};


void
pmc_init_color(void)
{
	pmc_color_t bg, fg;

	for (bg = C_BLACK; bg <= C_WHITE; bg++) {
		for (fg = C_BLACK; fg <= C_WHITE; fg++) {
			//printw("Init color %02x\n", BG(bg)|fg);
			init_pair((short)(BG(bg)|fg),
				  pmc_to_curses[fg],
				  pmc_to_curses[bg]);
		}
	}
}
		
