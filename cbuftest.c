#include <stdio.h>
#include <sys/types.h>
#include <cbuf.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <malloc.h>

int
main(int argc, char **argv)
{
	char *buf;
	char *buffer;
	size_t bufsz;
	size_t blen;

	if (argc < 2) {
		printf("usage: %s size\n", argv[0]);
		return 1;
	}

	bufsz = cb_minsize() + atoi(argv[1]);

	buf = malloc(bufsz);

	buffer = cb_init(buf, bufsz, &blen);

	cb_append(buffer, "abc", 3);
	printf("%s\n", buffer);
	cb_append(buffer, "abc", 3);
	printf("%s\n", buffer);
	cb_append(buffer, "12345678", 8);
	printf("%s\n", buffer);
	cb_discard(buffer, 2);
	printf("after discard: %s\n", buffer);
	cb_backspace(buffer, 3);
	printf("after backspace: %s\n", buffer);
	if (blen > 4) {
		printf("Delete %p %p %d\n", buffer, &buffer[1], 2);
		cb_delete(buffer, &buffer[1], 2);
		printf("after delete: %s\n", buffer);
	}
	printf("%s\n", buffer);
	cb_reset(buffer);
	cb_append(buffer,"a", 1);
	printf("%s\n", buffer);

	cb_destroy(buffer);
	free(buf);

	return 0;
}
