#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <cbuf.h>
#include <ncurses.h>
#include <stdlib.h>

#include <color.h>


static void
proc_color(char *buf, attr_t *attrs, short *pair)
{
	int c = atoi(buf);

	if (c < 0)
		return;

	if (c == 0) {
		*attrs &= ~(A_BOLD | A_UNDERLINE | A_BLINK);
		*pair = BG(C_BLACK)|C_WHITE;
		return;
	}

	if (c == 1) {
		*attrs |= A_BOLD;
		return;
	}

	if (c >= 30 && c <= 37) {
		c-=30;
		*pair &= (short)~7;
		*pair |= (short)(c);
		return;
	}

	if (c >= 40 && c <= 47) {
		c-=40;
		*pair &= (short)~(BG(7));
		*pair |= (short)(BG(c));
	}

}


static void
process_color(WINDOW *win, char *buffer, size_t len)
{
	char *curr, *next = NULL;
	attr_t attrs;
	short pair;

	/* Get current attrs */
	wattr_get(win, &attrs, &pair, NULL);

	while((curr = strtok_r(next?NULL:buffer, ";", &next))) {
		proc_color(curr, &attrs, &pair);
	}

	wattr_set(win, attrs, pair, NULL);
}


static void
process_clear(WINDOW *win, char *buffer, size_t len)
{
	switch (*buffer) {
	case 0:
	case '0':
		/* NOT IMPLEMENTED */
		break;
	case '1':
		wclrtoeol(win);
		break;
	case '2':
		wclear(win);
		break;
	}

	//wcolor_set(win, BG(C_BLACK)|C_RED, NULL);
	//wprintw(win,"Cleared.\n");
}

static void
process_cursor(WINDOW *win, char *buffer, size_t len)
{
	char *next = NULL;
	int y = 1, x = 1;

	if (strlen(buffer)) {
		next = strchr(buffer, ';');
		if (next) {
			*next = 0;
			++next;
			x = atoi(next);
		}

		y = atoi(buffer);
	}

	x--; y--;

	if (y < 0)
		y = 0;
	if (x < 0)
		x = 0;

	wmove(win, y, x);
}

static void
process_buf(WINDOW *win, char *buffer, size_t len, char code)
{
	switch(code) {
	case 'm':
		process_color(win, buffer, len);
		break;
	case 'H':
		process_cursor(win, buffer, len);
		break;
	case 'J':
		process_clear(win, buffer, len);
		break;
	}
}


int
ansi_strip(char *circbuf, const char *buffer, size_t len)
{
	int x, ret = 0;
	int in_escape_code = 0;
	char c;

	for (x = 0; x < len; x++) {

		c = buffer[x];

		switch(buffer[x]) {
		case '\r':
			break;
		case 0:
			c = ' ';
			cb_append(circbuf, &c, 1);
			++ret;
			break;
		case 27:
			in_escape_code = 1;
			break;
		case '[':
			if (in_escape_code == 1) {
				in_escape_code = 2;
			} else if (in_escape_code == 0) {
				cb_append(circbuf, &c, 1);
				++ret;
			}
			break;
		/* most of these are not and will not be handled */
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'J':
		case 'K':
		case 'S':
		case 'T':
		case 'f':
		case 'm':
		case 's':
		case 'u':
			if (in_escape_code == 2) {
				in_escape_code = 0;
				continue;
			}
		default:
			if (in_escape_code == 2)
				continue;
			cb_append(circbuf, &c, 1);
			++ret;
			break;
		}
	}

	return ret;
}


size_t
ansi_waddnstr(WINDOW *win, const char *buffer, size_t len)
{
	int x;
	int in_escape_code = 0;
	char *presumed_start = NULL;
	char buf[80];
	size_t ret = 0;

	for (x = 0; x < len; x++) {

		switch(buffer[x]) {

		case '\r':
			++ret;
			break;
		case 0:
			waddch(win, ' ');
			++ret;
			break;
		case 27:
			in_escape_code = 1;
			presumed_start = (char *)&buffer[x];
			break;
		case '[':
			if (in_escape_code == 1) {
				in_escape_code = 2;
			} else if (in_escape_code == 0) {
				waddch(win, buffer[x]);
				++ret;
				//printf("%c\n", buffer[x]);
			}
			break;
		/* most of these are not and will not be handled */
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'J':
		case 'K':
		case 'S':
		case 'T':
		case 'f':
		case 'm':
		case 's':
		case 'u':
			if (in_escape_code == 2) {
				in_escape_code = 0;
				presumed_start += 2;
				if (&buffer[x] - presumed_start <= sizeof(buf)) {
					memcpy(buf, presumed_start, &buffer[x] - presumed_start);
					buf[&buffer[x] - presumed_start] = 0;
					process_buf(win, buf, strlen(buf), buffer[x]);
					ret += (&buffer[x] - presumed_start)+3;
				}
				continue;
			}
		default:
			if (in_escape_code)
				continue;
			++ret;
			waddch(win, buffer[x]);
			break;
		}
	}

	return ret;
}
