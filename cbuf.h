#ifndef _CBUF_H
#define _CBUF_H

int cb_append(char *buffer, const char *data, size_t len);
char *cb_strcat(char *buffer, const char *data);
char *cb_init(void *start, size_t len, size_t *retlen);
int cb_reset(char *buffer);
int cb_discard(char *buffer, size_t count);
int cb_delete(char *buffer, char *spot, size_t count);
int cb_backspace(char *buffer, size_t count);
void *cb_destroy(char *buffer);
void cb_free(char *buffer);
size_t cb_length(char *buffer);
size_t cb_minsize(void);

#endif
