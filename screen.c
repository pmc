#include <ncurses.h>
#include <malloc.h>
#include <string.h>
#include <cbuf.h>
#include <color.h>
#include <screen.h>
#include <ansi.h>

#include <list.h>

#include <debug.h>

#define MS_MAGIC 0x131948dd

#define HIST_SIZE 30

struct hist_node {
	list_head();
	char *hist;
};

struct mcl_screen {
	int magic;
	int pad;
	int xsize, ysize;
	WINDOW *background;
	WINDOW *output;
	WINDOW *bar_top;
	WINDOW *bar_bot;
	WINDOW *input;
	char *buffer;
	char *ibuf;
	char *scrollback;
	struct hist_node *hist_list;
	struct hist_node *hist_curr;
	int hp;
	int mhp;
	int en;
	int men;
	int mv;
	int mmv;
};


void
add_hist(struct hist_node **head, const char *val)
{
	char *vd;
	struct hist_node *c, *f = NULL;
	int len;

	if (val[0] == 0 || val[0] == '\n')
		return;

	list_for(head, c, len) {
		if (len >= HIST_SIZE) {
			list_remove(head, c);
			free(c->hist);
			free(c);
			break;
		}

		if (!strcmp(c->hist, val)) {
			list_remove(head, c);
			f = c;
			break;
		}
	}

	if (f) {
		list_prepend(head, f);
		return;
	}

	vd = strdup(val);
	if (!vd)
		return;
	len = strlen(vd);
	if (vd[len] == '\n')
		vd[len] = 0;
	f = malloc(sizeof(*f));
	if (!f) {
		free(vd);
		return;
	}

	f->hist = vd;
	list_prepend(head, f);
}


int
pmc_buffer(void *s, const char *data, size_t len)
{
	struct mcl_screen *scr = (struct mcl_screen *)s;
	if (scr->magic != MS_MAGIC)
		return -1;

	cb_append(scr->buffer, data, len);
	return 0;
}


char *
pmc_get_scrollback(void *s)
{
	struct mcl_screen *scr = (struct mcl_screen *)s;
	if (scr->magic != MS_MAGIC)
		return NULL;

	return scr->scrollback;
}


/* Zaps the contents of a bar */
void
pmc_write_bar(void *sp, int bar, const char *str)
{
	struct mcl_screen *scr = (struct mcl_screen *)sp;
	WINDOW *w;
	char *s;
	int x, wx, wy, len;
	
	if (scr->magic != MS_MAGIC)
		return;

	/* only go until first newline */
	s = strchr(str, '\n');
	if (!s) {
		len = strlen(str);
	} else {
		len = s - str;
	}

	//debug_print("Writing %d bytes to window\n", len);
	/* XXX TODO internal function below here */

	if (bar == BAR_TOP)
		w = scr->bar_top;
	else
		w = scr->bar_bot;

	/* zero out the rest */

	getmaxyx(w, wy, wx);
	mvwaddnstr(w, 0, 0, str, len);

	for (x = len; x < wx; x++)
		mvwaddch(w, 0, x, ' ');

	wnoutrefresh(w);
}


int
pmc_copyout(void *s, char *circbuf)
{
	struct mcl_screen *scr = (struct mcl_screen *)s;
	if (scr->magic != MS_MAGIC)
		return -1;

	cb_append(circbuf, scr->ibuf, cb_length(scr->ibuf));
	cb_reset(scr->ibuf);
	return 0;
}


static int
check_triggers(struct mcl_screen *scr, char *line)
{
	char *c;

	//debug_print("Line: %s", line);

	if (!strcmp(line,"+=--------------------------------------------------------------------------=+\n")) {
		for (c = line - 3; c >= scr->scrollback; c--) {
			if (*c == '|')
				break;
			if (*c == '\n') {
				while (*c == '\n' || *c == '\r' || *c == ' ' || *c == '\t')
					c++;
				
				pmc_write_bar(scr, BAR_TOP, c);
				return 0;
			}
		}
	}

	return 0;
}


int
pmc_update(void *s)
{
	struct mcl_screen *scr = (struct mcl_screen *)s;
	size_t len, olen, slen;
	int x, newline = 1;
	int y, data = 0;
	char *line;

	if (scr->magic != MS_MAGIC)
		return -1;

	while ((len = cb_length(scr->buffer))) {

		newline = 0;

		for (x = 0; x < len; x++) {
			if (scr->buffer[x] == '\n') {
				newline = 1;
				break;
			}
		}

		if (!newline)
			break;

		olen = x + 1;
		slen = ansi_strip(scr->scrollback, scr->buffer, olen);
		line = &scr->scrollback[cb_length(scr->scrollback) - slen];
		check_triggers(scr, line);
		olen = ansi_waddnstr(scr->output, scr->buffer, olen);
		cb_discard(scr->buffer, olen);
		++data;
	}

	/* add trailing non-line */
	len = cb_length(scr->buffer);
	if (len) {
		++data;
		olen = ansi_waddnstr(scr->output, scr->buffer, len);
		cb_discard(scr->buffer, olen);
	}

	if (data)
		wnoutrefresh(scr->output);
	wnoutrefresh(scr->bar_bot);
	wnoutrefresh(scr->bar_top);

	/* Hack to keep the cursor in the input box */
	getyx(scr->input, y, x);
	mvwaddch(scr->input, y, x,' ');
	wmove(scr->input, y, x);
	wnoutrefresh(scr->input);
	
	return 0;
}


void
pmc_resize(void *s)
{
	struct mcl_screen *scr = (struct mcl_screen *)s;
	int x;

	if (scr->magic != MS_MAGIC)
		return;

	endwin();
	refresh();

	getmaxyx(scr->background, scr->ysize, scr->xsize);

	wresize(scr->bar_top, 1, scr->xsize);
	mvwin(scr->bar_top, 0, 0);
	for (x = 0; x < scr->xsize; x++)
		mvwaddch(scr->bar_top, 0, x, ' ');

	wresize(scr->output, scr->ysize - 3, scr->xsize);
	mvwin(scr->output, 1, 0);

	wresize(scr->bar_bot, 1, scr->xsize);
	mvwin(scr->bar_bot, scr->ysize - 2, 0);
	for (x = 0; x < scr->xsize; x++)
		mvwaddch(scr->bar_bot, 0, x, ' ');

	wresize(scr->input, 1, scr->xsize);
	mvwin(scr->input, scr->ysize - 1, 0);

	refresh();
}


void *
pmc_init(void)
{
	struct mcl_screen *s;
	attr_t attr;
	short pair;
	int x;

	s = malloc(sizeof(*s));
	memset(s, 0, sizeof(*s));

	s->magic = MS_MAGIC;

	s->background = initscr();
	clearok(s->background, FALSE);

	start_color();
	pmc_init_color();
	noecho();

	getmaxyx(s->background, s->ysize, s->xsize);
	s->bar_top = newwin(1, s->xsize, 0, 0);
	idlok(s->bar_top, TRUE);

	wattr_get(s->bar_top, &attr, &pair, NULL);
	attr &= ~(A_UNDERLINE | A_BLINK);
	pair = BG(C_BLUE)|C_WHITE;
	wattr_set(s->bar_top, attr, pair, NULL);

	for (x = 0; x < s->xsize; x++)
		mvwaddch(s->bar_top, 0, x, ' ');

	s->output = newwin(s->ysize - 3, s->xsize, 1, 0);
	scrollok(s->output, TRUE);
	idlok(s->output, TRUE);

	s->bar_bot = newwin(1, s->xsize, s->ysize-2, 0);

	wattr_get(s->bar_bot, &attr, &pair, NULL);

	attr &= ~(A_UNDERLINE | A_BLINK);
	pair = BG(C_BLUE)|C_WHITE;
	wattr_set(s->bar_bot, attr, pair, NULL);

	for (x = 0; x < s->xsize; x++)
		mvwaddch(s->bar_bot, 0, x, ' ');

	s->input = newwin(1, s->xsize, s->ysize-1, 0);
	leaveok(s->input, FALSE);
	keypad(s->input, TRUE);

	s->buffer = malloc(4096);
	s->buffer = cb_init(s->buffer, 4096, NULL);
	s->scrollback = malloc(16384);
	s->scrollback = cb_init(s->scrollback, 16384, NULL);

	s->ibuf = malloc(1024);
	s->ibuf = cb_init(s->ibuf, 1024, NULL);

	refresh();

	return s;
}


static void 
_erase_word(char *buff)
{
	int x, y;

	x = cb_length(buff);
	y = x;
	x--;

	while (x >= 0 && (buff[x] == ' ' || buff[x] == '\t'))
		x--;
	while (x >= 0 && (buff[x] != ' ' && buff[x] != '\t'))
		x--;

	if (x < 0)
		cb_reset(buff);
	else
		cb_delete(buff, &buff[x+1], y-x);
}


/* This prevents a full screen refresh */
static void
zap_line(WINDOW *w, int line)
{
	wmove(w, line, 0);
	wclrtoeol(w);
	wmove(w, line, 0);
}


/* Wait for keyboard input */
int
pmc_listen(void *s)
{
	int keystroke;
	struct mcl_screen *scr = (struct mcl_screen *)s;
	char key = '\n';
	int x,y, ret = 0;

	if (scr->magic != MS_MAGIC)
		return -1;

	keystroke = wgetch(scr->input);
	key = (char)(keystroke & 0x0ff);

	switch(keystroke) {
	case KEY_ENTER:
	case '\n':
		/* reset this */
		scr->hist_curr = NULL;

		/* ^/ = local command; don't send */
		zap_line(scr->input, 0);

		/* add or move history to LRU */
		add_hist(&scr->hist_list, scr->ibuf);

		cb_append(scr->ibuf, &key, 1);
		if (*scr->buffer != '/')
			cb_append(scr->buffer, &key, 1);
		ret = 1;
		break;

	case '\x10':
	case KEY_UP:
		if (scr->hist_curr) {
			scr->hist_curr = (void *)(le(scr->hist_curr)->le_next);
		} else {
			if (scr->hist_list) {
				scr->hist_curr = scr->hist_list;
			} else {
				break;
			}
		}

		cb_reset(scr->ibuf);
		cb_strcat(scr->ibuf, scr->hist_curr->hist);
		zap_line(scr->input, 0);
		mvwaddstr(scr->input, 0, 0, scr->ibuf);

		break;

	case '\x0e':
	case KEY_DOWN:
		if (scr->hist_curr) {
			scr->hist_curr = (void *)(le(scr->hist_curr)->le_prev);
		} else {
			if (scr->hist_list) {
				scr->hist_curr = (void *)(le(scr->hist_list)->le_prev);
			} else {
				break;
			}
		}

		cb_reset(scr->ibuf);
		cb_strcat(scr->ibuf, scr->hist_curr->hist);
		zap_line(scr->input, 0);
		mvwaddstr(scr->input, 0, 0, scr->ibuf);

		break;

	case KEY_BACKSPACE:
	case '\x7f':
		cb_backspace(scr->ibuf, 1);
		/* optimization- draw a space over previous char */
		getyx(scr->input, y, x);
		mvwaddch(scr->input, y, x-1, ' ');
		wmove(scr->input, y, x-1);
		break;

	case '\x17':
		/* Erase to last space */
		_erase_word(scr->ibuf);
		zap_line(scr->input, 0);
		mvwaddstr(scr->input, 0, 0, scr->ibuf);
		break;

	case '\x15':
		cb_reset(scr->ibuf);
		zap_line(scr->input, 0);
		break;

	default:
		key = (char) keystroke;
		if (key == '\x3') {
			return 2;
		}
		cb_append(scr->ibuf, &key, 1);
		waddch(scr->input, key);
	}
	wnoutrefresh(scr->input);

	return ret;
}
