#ifndef _SCREEN_H
#define _SCREEN_H

#define BAR_TOP 0
#define BAR_BOTTOM 1

int pmc_buffer(void *s, const char *data, size_t len);
int pmc_copyout(void *s, char *circbuf); /* copy input out to circular buffer */
int pmc_update(void *s);
void * pmc_init(void);
int pmc_listen(void *s);
void pmc_resize(void *s);
void pmc_write_bar(void *s, int bar, const char *val);
char * pmc_get_scrollback(void *s);

#endif
